#Note: This script assumes that you have already set up a Telegram bot and have the necessary API token and Chat ID. You will also need to install the requests library if you don't already have it installed.
#
#This script can be run periodically (e.g., daily or weekly) to check the status of the certificate and notify you if it has expired.

import requests
import json
import datetime
import os

# Telegram Bot API token
BOT_TOKEN = "your_token_here"

# Chat ID of the recipient
CHAT_ID = "your_chat_id_here"

# Domain for which you want to check the certificate
domain = "example.com"

# Get the current date and time
now = datetime.datetime.now()

# Make a request to the Let's Encrypt API to get the certificate information for the domain
url = f"https://api.crt.sh/?q={domain}"
response = requests.get(url)

# Extract the certificate expiration date from the API response
cert_expiry = response.json()[0]["not_after"]
cert_expiry = datetime.datetime.strptime(cert_expiry, '%Y-%m-%d %H:%M:%S')

# Check if the certificate has already expired
if now > cert_expiry:
    # If the certificate has expired, send a message to the Telegram recipient
    message = f"The Let's Encrypt certificate for {domain} has expired on {cert_expiry}"
    send_message_url = f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage?chat_id={CHAT_ID}&text={message}"
    requests.get(send_message_url)
